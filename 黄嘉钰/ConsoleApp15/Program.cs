﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp15
{
    class Program
    {
        static void Main(string[] args)
        {
            /*完成AdoNet两种获取数据的方法；
              完成增加、删除、更新的方法的演练；*/

            string conString = "server = .;uid = sa; pwd = seventeen111; database = StudentInfoDB";
            //string conString = "Data Source = . ; Initial Catalog = StudentInfoDB ; User ID = sa ; Password = seventeen111";

            //创建 SqlConnection 类的实例
            SqlConnection sqlConnection = new SqlConnection(conString);

            //打开数据库连接
            sqlConnection.Open();

                String cmdString = "select * from Students";

                Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t", "id", "name", "gender", "score","class_id");

                DataTable dataTable = new DataTable();

                SqlDataAdapter dataAdapter = new SqlDataAdapter(cmdString, conString);

                dataAdapter.Fill(dataTable);

                foreach (DataRow row in dataTable.Rows)
                {
                    Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t", row["id"], row["name"], row["gender"], row["score"],row["class_id"]);
                }

            //添加
            SqlCommand command = new SqlCommand("select * from Students",sqlConnection);

            command.CommandText = "insert into Students values('小八','M',88,5)";
            command.ExecuteNonQuery();

            //删除
            command.CommandText = "delete from Students where id = 16";
            command.ExecuteNonQuery();

            //更新
            dataAdapter.Update(dataTable);

            sqlConnection.Close();

                





        }
    }
}
