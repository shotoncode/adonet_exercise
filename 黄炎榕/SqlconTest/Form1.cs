﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SqlconTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //连接字符串
        public static string constr = "Server=.;user Id=sa;pwd=123456;database=Test;";
        //加载驱动
        public static SqlConnection sqlcon = new SqlConnection(constr);
        //加载程序的时候判断数据库是否关闭并自动打开数据库
        private void Form1_Load(object sender, EventArgs e)
        {
            if (sqlcon.State == ConnectionState.Closed)
            {
                sqlcon.Open();
            }
        }
        //增加
        private void btn_Insert_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
            form2.BringToFront();//窗体显示在最前端
        }
        //删除
        private void btn_Del_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(this.txt_Id.Text);
            string name = this.txt_Name.Text;
            string sql = "delete from stuInfo where Id=" + id + " and Name='" + name + "'";
            SqlCommand command = new SqlCommand(sql, sqlcon);
            int result = command.ExecuteNonQuery();
            if (result >= 1)
            {
                MessageBox.Show("删除成功");
            }
            else
            {
                MessageBox.Show("删除失败");
            }
        }
        //修改
        private void btn_Update_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(this.txt_Id.Text);
            string name = this.txt_Name.Text;
            string sql = "update StuInfo set Name='"+name+"' where Id="+id+"";
            SqlCommand command = new SqlCommand(sql,sqlcon);
            int result = command.ExecuteNonQuery();
            if (result>=1)
            {
                MessageBox.Show("修改成功");
            }
            else
            {
                MessageBox.Show("修改失败");
            }
        }
        //查询
        private void btn_Select_Click(object sender, EventArgs e)
        {
            string id = this.txt_Id.Text;
            if (id == "")
            {
                string sql = "select * from stuInfo";
                SqlDataAdapter sqlAda = new SqlDataAdapter(sql, sqlcon);
                DataTable dt = new DataTable();
                sqlAda.Fill(dt);
                this.dataGridView1.DataSource = dt;
            }
            else
            {
                string sql = "select * from stuInfo where Id=" + id + "";
                SqlDataAdapter sqlAda = new SqlDataAdapter(sql, sqlcon);
                DataTable dt = new DataTable();
                sqlAda.Fill(dt);
                this.dataGridView1.DataSource = dt;
            }
        }
    }
}

