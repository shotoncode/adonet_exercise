﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace AdoNet
{
    class Program
    {
        static void Main(string[] args)
        {

            string sql1 = "select * from UserPwd";

            //读取数据

            Operation.DataRead(sql1);//法一

            Operation.Adapter(sql1);//法二


            //增加、输出和更新数据

            string sql2 = "insert into UserPwd values('胡哥',454566)";

            string sql3 = "update UserPwd set pwd='huge222' where name='胡哥'";

            string sql4 = "delete UserPwd where name='胡哥'";

            Operation.Aelaw(sql2);
            Operation.Aelaw(sql3);
            Operation.Aelaw(sql4);
        }



    }
}
