﻿using System;
using System.Data.SqlClient;
using Xceed.Wpf.Toolkit;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var address = "server=.;database=test;uid=sa;pwd=123456";//编写数据库连接串
            SqlConnection sql = new SqlConnection(address);//建立连接
            sql.Open();//打开数据库连接
            var select = "select*from UserInfo";
            SqlCommand command = new SqlCommand(select,sql);//对数据库进行增删改查等操作
            var reader = command.ExecuteReader();
            Console.WriteLine("{0}\t{1}\t{2}\t{3}\t","用户编号", "用户名", "年龄", "手机号码");
            while (reader.Read())
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}\t", reader["UserId"], reader["UserName"],reader["UserAge"], reader["UserNumber"]);
            }
            sql.Close();//关闭数据库连接


            }
        }
    }

